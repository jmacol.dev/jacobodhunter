import { SecundariaComponent } from './core/components/page/propuesta/secundaria/secundaria.component';
import { PrimariaComponent } from './core/components/page/propuesta/primaria/primaria.component';
import { ApoyaComponent } from './core/components/page/apoya/apoya.component';
import { ComunicadosComponent } from './core/components/page/comunicados/comunicados.component';
import { PropuestaComponent } from './core/components/page/propuesta/propuesta.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthCoverLoginComponent } from './auth/auth-cover-login/auth-cover-login.component';
import { AuthCoverRePasswordComponent } from './auth/auth-cover-re-password/auth-cover-re-password.component';

import { IndexComponent } from './core/components/index/index.component';
import { MasterPageComponent } from './core/components/master-page/master-page.component';
import { PageErrorComponent } from './core/components/page-error/page-error.component';
import { PageTermsComponent } from './core/components/page-terms/page-terms.component';
import { combineLatest } from 'rxjs/internal/operators';
import { AdmisionComponent } from './core/components/page/admision/admision.component';
import { ContactanosComponent } from './core/components/page/contactanos/contactanos.component';

const routes: Routes = [
  {
    path: '',
    component: MasterPageComponent,
    children: [
      { path: '', component: IndexComponent },
      { path: 'admision', component: AdmisionComponent },
      { path: 'propuesta', component: PropuestaComponent },
      { path: 'primaria', component: PrimariaComponent },
      { path: 'secundaria', component: SecundariaComponent },
      { path: 'comunicados', component: ComunicadosComponent },
      { path: 'contactanos', component: ContactanosComponent },
      { path: 'apoya-a-un-niño', component: ApoyaComponent },
      { path: 'index', component: IndexComponent },
      { path: 'page-terms', component: PageTermsComponent },
    ]
  },


  { path: 'administrador', component: AuthCoverLoginComponent },
  { path: 'recuperar-password', component: AuthCoverRePasswordComponent },
  { path: 'page-error', component: PageErrorComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: "enabled",
  scrollOffset: [0, 0],onSameUrlNavigation: 'reload',
  // Enable scrolling to anchors
  anchorScrolling: "enabled"})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
