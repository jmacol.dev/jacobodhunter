import { PropertiesService } from './../../../../properties.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admision',
  templateUrl: './admision.component.html',
  styleUrls: ['./admision.component.css']
})
export class AdmisionComponent implements OnInit {

  constructor(private modalService: NgbModal,
  public properties: PropertiesService) { }

  ngOnInit(): void {
  }

  terminos(content) {
    this.modalService.open(content, { windowClass: 'dark-modal', size: 'lg', centered: true })
  }

  public async enviar(captchaResponse: string) {
    console.log(captchaResponse)
  }

}
