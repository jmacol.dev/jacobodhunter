import { Component, OnInit } from '@angular/core';
import { PropertiesService } from 'src/app/properties.service';

@Component({
  selector: 'app-contactanos',
  templateUrl: './contactanos.component.html',
  styleUrls: ['./contactanos.component.css']
})
export class ContactanosComponent implements OnInit {

  constructor(public properties: PropertiesService) { }

  ngOnInit(): void {
  }

  public async enviar(captchaResponse: string) {

  }

}
