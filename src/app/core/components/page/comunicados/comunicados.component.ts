import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comunicados',
  templateUrl: './comunicados.component.html',
  styleUrls: ['./comunicados.component.css']
})
export class ComunicadosComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  comunicados = [
    {
      "titulo": "TITULO COMUNICADO 001",
      "hora": "04:00PM - 05:00PM",
      "dia": "01",
      "mes": "ENE",
      "lugar": "Colegio Jacobo D: Hunter",
      "direccion": "Av. Brasilia 715 Hunter",
      "detalle" : "Se llevará a acabo la reunión de padres de familia.",
      "fechapublica":"14-01-2023",
      "documento": "doc"
    },
    {
      "titulo": "TITULO COMUNICADO 002",
      "hora": "04:00PM - 05:00PM",
      "dia": "01",
      "mes": "FEB",
      "lugar": "Colegio Jacobo D: Hunter",
      "direccion": "Av. Brasilia 715 Hunter",
      "detalle": "Se llevará a acabo la reunión de padres de familia.",
      "fechapublica":"14-01-2023",
      "documento": ""
    },
    {
      "titulo": "TITULO COMUNICADO 003",
      "hora": "04:00PM - 05:00PM",
      "dia": "01",
      "mes": "MAR",
      "lugar": "Colegio Jacobo D: Hunter",
      "direccion": "Av. Brasilia 715 Hunter",
      "detalle" : "Se llevará a acabo la reunión de padres de familia.",
      "fechapublica":"14-01-2023",
      "documento": "doc"
    },
    {
      "titulo": "TITULO COMUNICADO 004",
      "hora": "04:00PM - 05:00PM",
      "dia": "01",
      "mes": "ABR",
      "lugar": "Colegio Jacobo D: Hunter",
      "direccion": "Av. Brasilia 715 Hunter",
      "detalle" : "Se llevará a acabo la reunión de padres de familia.",
      "fechapublica":"14-01-2023",
      "documento": "doc"
    },
    {
      "titulo": "TITULO COMUNICADO 005",
      "hora": "04:00PM - 05:00PM",
      "dia": "01",
      "mes": "MAY",
      "lugar": "Colegio Jacobo D: Hunter",
      "direccion": "Av. Brasilia 715 Hunter",
      "detalle" : "Se llevará a acabo la reunión de padres de familia.",
      "fechapublica": "14-01-2023",
      "documento": ""
    },
    {
      "titulo": "TITULO COMUNICADO 006",
      "hora": "04:00PM - 05:00PM",
      "dia": "01",
      "mes": "JUN",
      "lugar": "Colegio Jacobo D: Hunter",
      "direccion": "Av. Brasilia 715 Hunter",
      "detalle" : "Se llevará a acabo la reunión de padres de familia.",
      "fechapublica":"14-01-2023",
      "documento": "doc"
    },
    
  ]

}
