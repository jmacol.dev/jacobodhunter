import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApoyaComponent } from './apoya.component';

describe('ApoyaComponent', () => {
  let component: ApoyaComponent;
  let fixture: ComponentFixture<ApoyaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApoyaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApoyaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
