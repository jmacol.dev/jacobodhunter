import { PropertiesService } from './../../../../properties.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-apoya',
  templateUrl: './apoya.component.html',
  styleUrls: ['./apoya.component.css']
})
export class ApoyaComponent implements OnInit {

  constructor(private modalService: NgbModal,
  public properties: PropertiesService) { }

  ngOnInit(): void {
  }

  memberData = [
    {
      profile: "assets/images/client/01.jpg",
      list: ["facebook", "instagram", "twitter", "linkedin"],
      name: "Nombres y Apellidos",
      designation: "Promotor"
    },
    {
      profile: "assets/images/client/04.jpg",
      list: ["facebook", "instagram", "twitter", "linkedin"],
      name: "Nombres y Apellidos",
      designation: "Director"
    },
    {
      profile: "assets/images/client/02.jpg",
      list: ["facebook", "instagram", "twitter", "linkedin"],
      name: "Nombres y Apellidos",
      designation: "Coordinador"
    },
    {
      profile: "assets/images/client/03.jpg",
      list: ["facebook", "instagram", "twitter", "linkedin"],
      name: "Nombres y Apellidos",
      designation: "Secretaria"
    }
  ];

  testimonialData = [
    {
      profile: "assets/images/client/01.jpg",
      name: "Thomas Israel",
      designation: "5° PRIMARIA",
      message: `" It seems that only fragments of the original text remain in the Lorem Ipsum texts used today. "`
    },
    {
      profile: "assets/images/client/02.jpg",
      name: "Barbara McIntosh",
      designation: "5° PRIMARIA",
      message: `" One disadvantage of Lorum Ipsum is that in Latin certain letters appear more frequently than others. "`
    },
    {
      profile: "assets/images/client/03.jpg",
      name: "Carl Oliver",
      designation: "5° PRIMARIA",
      message: `" The most well-known dummy text is the 'Lorem Ipsum', which is said to have originated in the 16th century. "`
    },
    {
      profile: "assets/images/client/04.jpg",
      name: "Christa Smith",
      designation: "5° PRIMARIA",
      message: `" According to most sources, Lorum Ipsum can be traced back to a text composed by Cicero. "`
    },
    {
      profile: "assets/images/client/05.jpg",
      name: "Dean Tolle",
      designation: "5° PRIMARIA",
      message: `" There is now an abundance of readable dummy texts. These are usually used when a text is required. "`
    },
    {
      profile: "assets/images/client/06.jpg",
      name: "Jill Webb",
      designation: "5° PRIMARIA",
      message: `" Thus, Lorem Ipsum has only limited suitability as a visual filler for German texts. "`
    }
  ];

  workList = [
    {
      image: 'assets/images/personal/1.jpg',
      title: 'Iphone mockup',
      category: 'Branding'
    },
    {
      image: 'assets/images/personal/2.jpg',
      title: 'Mockup Collection',
      category: 'Mockup'
    },
    {
      image: 'assets/images/personal/3.jpg',
      title: 'Abstract images',
      category: 'Abstract'
    },
    {
      image: 'assets/images/personal/4.jpg',
      title: 'Yellow bg with Books',
      category: 'Books'
    },
    {
      image: 'assets/images/personal/5.jpg',
      title: 'Company V-card',
      category: 'V-card'
    },
    {
      image: 'assets/images/personal/6.jpg',
      title: 'Mockup box with paints',
      category: 'Photography'
    },
    {
      image: 'assets/images/personal/5.jpg',
      title: 'Company V-card',
      category: 'V-card'
    },
    {
      image: 'assets/images/personal/6.jpg',
      title: 'Mockup box with paints',
      category: 'Photography'
    }
  ];


  openWindowCustomClass(content) {
    this.modalService.open(content, { windowClass: 'dark-modal', size: 'lg', centered: true });
  }

  contacto(content) {
    this.modalService.open(content, { windowClass: 'dark-modal', size: 'lg', centered: true })
  }

  public async enviar(captchaResponse: string) {
    console.log(captchaResponse)
  }
}
