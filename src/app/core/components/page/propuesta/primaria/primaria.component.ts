import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-primaria',
  templateUrl: './primaria.component.html',
  styleUrls: ['./primaria.component.css']
})
export class PrimariaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  areas = [
    { "area": "Arte y Cultura" },
    { "area": "Ciencia y Tecnología" },
    { "area": "Comunicación" },
    { "area": "Educación Física" },
    { "area": "Formación Cristiana" },
    { "area": "Matemática" },
    { "area": "Personal Social" },
  ];

  talleres = [
    { "taller": "Inglés" },
    { "taller": "Cómputo" }
  ]

}
