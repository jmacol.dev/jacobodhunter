import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-secundaria',
  templateUrl: './secundaria.component.html',
  styleUrls: ['./secundaria.component.css']
})
export class SecundariaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  areas = [
    { "area": "Arte y Cultura", "subareas" : [] },
    
    { "area": "Ciencias Sociales", "subareas" : [] },
    { "area": "Comunicación", "subareas" : [] },
    
    { "area": "Educación Física", "subareas" : [] },
    { "area": "Educación para el Trabajo", "subareas" : [] },
    { "area": "Formación Cristiana", "subareas" : [] },
    { "area": "Inglés", "subareas" : [] },
    { "area": "Matemática", "subareas": [] },  
    { "area": "Ciencia y Tecnología", "subareas": [{ "subarea": "Biología" }, { "subarea": "Química" }, {"subarea": "Física" }] },
    { "area": "Desarrollo Personal, Cívica y Ciudadanía", "subareas" : [] },
  ];

  talleres = [
    { "taller": "Inglés" },
    { "taller": "Cómputo" }
  ]

}
