import { TestimonialComponent } from './testimonial/testimonial.component';
import { MemberComponent } from './member/member.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { RouterModule } from '@angular/router';

import { ScrollspyDirective } from './scrollspy.directive';
import { FeatherModule } from 'angular-feather';

@NgModule({
  declarations: [
    ScrollspyDirective,
    MemberComponent,
    TestimonialComponent
  ],
  imports: [
    CommonModule,
    CarouselModule,
    FeatherModule,
    RouterModule
  ],
  exports: [
    MemberComponent,
    TestimonialComponent
  ]
})

export class SharedModule { }
