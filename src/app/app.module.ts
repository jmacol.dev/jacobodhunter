
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LightboxModule } from 'ngx-lightbox';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { NgxTypedJsModule } from 'ngx-typed-js';
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CountToModule } from 'angular-count-to';
import { NgxMasonryModule } from 'ngx-masonry';

import { SharedModule } from "./shared/shared.module";

import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';


import { MasterPageComponent } from './core/components/master-page/master-page.component';
import { AuthCoverLoginComponent } from './auth/auth-cover-login/auth-cover-login.component';
import { AuthCoverRePasswordComponent } from './auth/auth-cover-re-password/auth-cover-re-password.component';
import { IndexComponent } from './core/components/index/index.component';
import { PageErrorComponent } from './core/components/page-error/page-error.component';
import { PageTermsComponent } from './core/components/page-terms/page-terms.component';
import { FeatherModule } from 'angular-feather';

import { Activity
,Airplay
,AlertCircle
,AlertOctagon
,AlertTriangle
,AlignCenter
,AlignJustify
,AlignLeft
,AlignRight
,Anchor
,Aperture
,Archive
,ArrowDownCircle
,ArrowDownLeft
,ArrowDownRight
,ArrowDown
,ArrowLeftCircle
,ArrowLeft
,ArrowRightCircle
,ArrowRight
,ArrowUpCircle
,ArrowUpLeft
,ArrowUpRight
,ArrowUp
,AtSign
,Award
,BarChart2
,BarChart
,BatteryCharging
,Battery
,BellOff
,Bell
,Bluetooth
,Bold
,BookOpen
,Book
,Bookmark
,Box
,Briefcase
,Calendar
,CameraOff
,Camera
,Cast
,CheckCircle
,CheckSquare
,Check
,ChevronDown
,ChevronLeft
,ChevronRight
,ChevronUp
,ChevronsDown
,ChevronsLeft
,ChevronsRight
,ChevronsUp
,Chrome
,Circle
,Clipboard
,Clock
,CloudDrizzle
,CloudLightning
,CloudOff
,CloudRain
,CloudSnow
,Cloud
,Code
,Codepen
,Codesandbox
,Coffee
,Columns
,Command
,Compass
,Copy
,CornerDownLeft
,CornerDownRight
,CornerLeftDown
,CornerLeftUp
,CornerRightDown
,CornerRightUp
,CornerUpLeft
,CornerUpRight
,Cpu
,CreditCard
,Crop
,Crosshair
,Database
,Delete
,Disc
,DollarSign
,DownloadCloud
,Download
,Droplet
,Edit2
,Edit3
,Edit
,ExternalLink
,EyeOff
,Eye
,Facebook
,FastForward
,Feather
,Figma
,FileMinus
,FilePlus
,FileText
,File
,Film
,Filter
,Flag
,FolderMinus
,FolderPlus
,Folder
,Frown
,Gift
,GitBranch
,GitCommit
,GitMerge
,GitPullRequest
,Github
,Gitlab
,Globe
,Grid
,HardDrive
,Hash
,Headphones
,Heart
,HelpCircle
,Hexagon
,Home
,Image
,Inbox
,Info
,Instagram
,Italic
,Key
,Layers
,Layout
,LifeBuoy
,Link2
,Link
,Linkedin
,List
,Loader
,Lock
,LogIn
,LogOut
,Mail
,MapPin
,Map
,Maximize2
,Maximize
,Meh
,Menu
,MessageCircle
,MessageSquare
,MicOff
,Mic
,Minimize2
,Minimize
,MinusCircle
,MinusSquare
,Minus
,Monitor
,Moon
,MoreHorizontal
,MoreVertical
,MousePointer
,Move
,Music
,Navigation2
,Navigation
,Octagon
,Package
,Paperclip
,PauseCircle
,Pause
,PenTool
,Percent
,PhoneCall
,PhoneForwarded
,PhoneIncoming
,PhoneMissed
,PhoneOff
,PhoneOutgoing
,Phone
,PieChart
,PlayCircle
,Play
,PlusCircle
,PlusSquare
,Plus
,Pocket
,Power
,Printer
,Radio
,RefreshCcw
,RefreshCw
,Repeat
,Rewind
,RotateCcw
,RotateCw
,Rss
,Save
,Scissors
,Search
,Send
,Server
,Settings
,Share2
,Share
,ShieldOff
,Shield
,ShoppingBag
,ShoppingCart
,Shuffle
,Sidebar
,SkipBack
,SkipForward
,Slack
,Slash
,Sliders
,Smartphone
,Smile
,Speaker
,Square
,Star
,StopCircle
,Sun
,Sunrise
,Sunset
,Tablet
,Tag
,Target
,Terminal
,Thermometer
,ThumbsDown
,ThumbsUp
,ToggleLeft
,ToggleRight
,Trash2
,Trash
,Trello
,TrendingDown
,TrendingUp
,Triangle
,Truck
,Tv
,Twitter
,Type
,Umbrella
,Underline
,Unlock
,UploadCloud
,Upload
,UserCheck
,UserMinus
,UserPlus
,UserX
,User
,Users
,VideoOff
,Video
,Voicemail
,Volume1
,Volume2
,VolumeX
,Volume
,Watch
,WifiOff
,Wifi
,Wind
,XCircle
,XOctagon
,XSquare
,X
,Youtube
,ZapOff
,Zap
,ZoomIn
,ZoomOut } from 'angular-feather/icons';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PropuestaComponent } from './core/components/page/propuesta/propuesta.component';
import { ComunicadosComponent } from './core/components/page/comunicados/comunicados.component';
import { ApoyaComponent } from './core/components/page/apoya/apoya.component';
import { ContactanosComponent } from './core/components/page/contactanos/contactanos.component';
import { AdmisionComponent } from './core/components/page/admision/admision.component';
import { PrimariaComponent } from './core/components/page/propuesta/primaria/primaria.component';
import { SecundariaComponent } from './core/components/page/propuesta/secundaria/secundaria.component';
import { RecaptchaModule } from 'ng-recaptcha';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};

const allIcons = {
  Activity
  , Airplay
  , AlertCircle
  , AlertOctagon
  , AlertTriangle
  , AlignCenter
  , AlignJustify
  , AlignLeft
  , AlignRight
  , Anchor
  , Aperture
  , Archive
  , ArrowDownCircle
  , ArrowDownLeft
  , ArrowDownRight
  , ArrowDown
  , ArrowLeftCircle
  , ArrowLeft
  , ArrowRightCircle
  , ArrowRight
  , ArrowUpCircle
  , ArrowUpLeft
  , ArrowUpRight
  , ArrowUp
  , AtSign
  , Award
  , BarChart2
  , BarChart
  , BatteryCharging
  , Battery
  , BellOff
  , Bell
  , Bluetooth
  , Bold
  , BookOpen
  , Book
  , Bookmark
  , Box
  , Briefcase
  , Calendar
  , CameraOff
  , Camera
  , Cast
  , CheckCircle
  , CheckSquare
  , Check
  , ChevronDown
  , ChevronLeft
  , ChevronRight
  , ChevronUp
  , ChevronsDown
  , ChevronsLeft
  , ChevronsRight
  , ChevronsUp
  , Chrome
  , Circle
  , Clipboard
  , Clock
  , CloudDrizzle
  , CloudLightning
  , CloudOff
  , CloudRain
  , CloudSnow
  , Cloud
  , Code
  , Codepen
  , Codesandbox
  , Coffee
  , Columns
  , Command
  , Compass
  , Copy
  , CornerDownLeft
  , CornerDownRight
  , CornerLeftDown
  , CornerLeftUp
  , CornerRightDown
  , CornerRightUp
  , CornerUpLeft
  , CornerUpRight
  , Cpu
  , CreditCard
  , Crop
  , Crosshair
  , Database
  , Delete
  , Disc
  , DollarSign
  , DownloadCloud
  , Download
  , Droplet
  , Edit2
  , Edit3
  , Edit
  , ExternalLink
  , EyeOff
  , Eye
  , Facebook
  , FastForward
  , Feather
  , Figma
  , FileMinus
  , FilePlus
  , FileText
  , File
  , Film
  , Filter
  , Flag
  , FolderMinus
  , FolderPlus
  , Folder
  , Frown
  , Gift
  , GitBranch
  , GitCommit
  , GitMerge
  , GitPullRequest
  , Github
  , Gitlab
  , Globe
  , Grid
  , HardDrive
  , Hash
  , Headphones
  , Heart
  , HelpCircle
  , Hexagon
  , Home
  , Image
  , Inbox
  , Info
  , Instagram
  , Italic
  , Key
  , Layers
  , Layout
  , LifeBuoy
  , Link2
  , Link
  , Linkedin
  , List
  , Loader
  , Lock
  , LogIn
  , LogOut
  , Mail
  , MapPin
  , Map
  , Maximize2
  , Maximize
  , Meh
  , Menu
  , MessageCircle
  , MessageSquare
  , MicOff
  , Mic
  , Minimize2
  , Minimize
  , MinusCircle
  , MinusSquare
  , Minus
  , Monitor
  , Moon
  , MoreHorizontal
  , MoreVertical
  , MousePointer
  , Move
  , Music
  , Navigation2
  , Navigation
  , Octagon
  , Package
  , Paperclip
  , PauseCircle
  , Pause
  , PenTool
  , Percent
  , PhoneCall
  , PhoneForwarded
  , PhoneIncoming
  , PhoneMissed
  , PhoneOff
  , PhoneOutgoing
  , Phone
  , PieChart
  , PlayCircle
  , Play
  , PlusCircle
  , PlusSquare
  , Plus
  , Pocket
  , Power
  , Printer
  , Radio
  , RefreshCcw
  , RefreshCw
  , Repeat
  , Rewind
  , RotateCcw
  , RotateCw
  , Rss
  , Save
  , Scissors
  , Search
  , Send
  , Server
  , Settings
  , Share2
  , Share
  , ShieldOff
  , Shield
  , ShoppingBag
  , ShoppingCart
  , Shuffle
  , Sidebar
  , SkipBack
  , SkipForward
  , Slack
  , Slash
  , Sliders
  , Smartphone
  , Smile
  , Speaker
  , Square
  , Star
  , StopCircle
  , Sun
  , Sunrise
  , Sunset
  , Tablet
  , Tag
  , Target
  , Terminal
  , Thermometer
  , ThumbsDown
  , ThumbsUp
  , ToggleLeft
  , ToggleRight
  , Trash2
  , Trash
  , Trello
  , TrendingDown
  , TrendingUp
  , Triangle
  , Truck
  , Tv
  , Twitter
  , Type
  , Umbrella
  , Underline
  , Unlock
  , UploadCloud
  , Upload
  , UserCheck
  , UserMinus
  , UserPlus
  , UserX
  , User
  , Users
  , VideoOff
  , Video
  , Voicemail
  , Volume1
  , Volume2
  , VolumeX
  , Volume
  , Watch
  , WifiOff
  , Wifi
  , Wind
  , XCircle
  , XOctagon
  , XSquare
  , X
  , Youtube
  , ZapOff
  , Zap
  , ZoomIn
  , ZoomOut
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MasterPageComponent,
    AuthCoverLoginComponent,
    AuthCoverRePasswordComponent,
    IndexComponent,
    PageErrorComponent,
    PageTermsComponent,
    AdmisionComponent,
    PropuestaComponent,
    ComunicadosComponent,
    ApoyaComponent,
    ContactanosComponent,
    PrimariaComponent,
    SecundariaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    RouterModule,
    CarouselModule,
    FeatherModule.pick(allIcons),
    ScrollToModule.forRoot(),
    RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }),
    NgxYoutubePlayerModule,
    NgbDropdownModule,
    CKEditorModule,
    NgbModule,
    NgbNavModule,
    FormsModule,
    SwiperModule,
    NgxTypedJsModule,
    FlatpickrModule.forRoot(),
    CountToModule,
    NgxMasonryModule,
    LightboxModule,
    SharedModule,
    RecaptchaModule
  ],
  exports: [
    FeatherModule,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
